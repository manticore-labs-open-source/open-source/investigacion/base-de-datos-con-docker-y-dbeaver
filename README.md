# Conexión a una base de datos MySQL utilizando Docker y DBeaver


## ¿Qué es un Docker?

Docker, como su nombre en inglés indica, es un contenedor el cual permite levantar múltiples máquinas virtuales para realizar el despliegue de distintas aplicaciones. Una de sus ventajas es que minimiza la instalación de servidores de distintas bases de datos, como por ejemplo Mongo, MySQL o Posgres. La instalación y configuración del programa, puede ser encontrada en la documentación oficial [aquí](https://docs.docker.com/get-started/).

## Levantamiento de un contenedor MySQL en Docker

Para crear una nueva conexión, para que sea más sencillo para el usuario, se debe utilizar *_Kitematic_* que es una interfaz para las conexiones con las que cuenta Docker; una vez abierta la aplicación, se seleccionará la conexión deseada, que en este caso será MySQL. Antes de dar clic en el botón *_create_* es necesario seleccionar la versión 5.6 por cuestión de estabilidad, al seleccionarla, se mostrará algo como esto:

![Version MySQL](./imagenes/version-mysql.png)

Con esto, se procede a dar clic ahora si, en el botón *_create_*. Se iniciará el contenedor MySQl y se mostrará que surge un error como se muestra a continuación:

![Error](./imagenes/error.png)

Esto se debe a que las variables de entorno necesarias para la correcta ejecución del contenedor aún no han sido configuradas. Para su respectiva configuración, es necesarrio dirigirse a la pestaña *_Settings_* y se mostrará los siguiente:

![Variables](./imagenes/variables.png)

Para la correcta configuración del contenedor, es necesario incluir, como mínimo la siguiente variable de entorno: 

```
MYSQL_ROOT_PASSWORD
```

La cual permitirá que se ejecute el contenedor de manera correcta. Como en esta guía se pretende realizar una conexión MySQL para realizar el levantamiento de una base de datos, es necesario configurar también, las siguientes variables de entorno:

```
MYSQL_DATABASE
MYSQL_USER
MYSQL_PASSWORD
```

Estas variables especifican el nombre de la base de datos y sus respectivas credenciales de usuario. Todas estas variables descritas, serán configuradas dentro de la pestaña *_Settings_* así:

![Configuracion de variables](./imagenes/configuracion-variables.png)

Cuando ya se esté seguro de que las variables estén correctamente configuradas, se debe dar clic en el botón **SAVE**. Luego de esto, es necesario volver a la pestaña *_Home_* y se podrá apreciar que se levanta de manera correcta el contenedor, mostrando detalles como el puerto en el cual está funcionado, como se muestra a continuación:

![Ejecucion del contenedor](./imagenes/ejecucion-docker.png)

## ¿Qué es DBeaver?

DBeaver es básicamente un gestor para bases de datos relacionales. Es un proyecto open source y por tanto, multiplataforma, el cuál puede ser descargado a través de su página oficial [aquí](https://dbeaver.io/download/). Su instalación, independientemente del sistema operativo, es sencilla y bastante inuitiva.

## Configuración de una conexión MySQL en DBeaver con Docker

Con lo explicado anteriormente, se puede utilizar el contenedor de Docker para realizar el levantamiento de una base de datos en DBeaver, para lo cual, el primer paso es abrir el programa y dar clic en **New Connection Wizzard** (el cual se encuentra bajo la opción File). Inmediatamente, aparecerá una ventana similar a la siguiente, en la cual, se debe escoger el tipo de base deseada, que en este caso será MySQL:

![Seleccion MySQL](./imagenes/seleccion-mysql.png)

Una vez escogida la opción, se debe dar clic en *_Next_* con lo que se mostrará una pantalla de configuración de conexión, la cual debe ser llenada con los mismos datos de las variables de entorno de Docker y especificando el puerto en el que está levantado el contenedor, así:

![Configuracion conexión](./imagenes/configuracion-dbeaver.png)

Para finalizar, se debe dar clic en el botón *_Finish_* y se creará la conexión a la base de datos. Dicha conexión podrá ser utilizada para su posterior uso con el framework Nestjs, por ejemplo.


<a href="https://twitter.com/following" target="_blank"><img alt="Sígueme en Twitter" height="35" width="35" src="https://3.bp.blogspot.com/-E4jytrbmLbY/XCrI2Xd_hUI/AAAAAAAAIAo/qXt-bJg1UpMZmTjCJymxWEOGXWEQ2mv3ACLcBGAs/s1600/twitter.png" title="Sígueme en Twitter"/> @BAndresTorres </a><br>
<a href="https://www.facebook.com/bryan.a.torres.5" target="_blank"><img alt="Sígueme en Facebook" height="35" width="35" src="https://sguru.org/wp-content/uploads/2018/02/Facebook-PNG-Image-71244.png" title="Sígueme en Facebook"/> Andrés Torres Albuja </a><br>
<a href="https://www.instagram.com/adler.luft/" target="_blank"><img alt="Sígueme en Instagram" height="35" width="35" src="https://4.bp.blogspot.com/-Ilxti1UuUuI/XCrIy6hBAcI/AAAAAAAAH_k/QV5KbuB9p3QB064J08W2v-YRiuslTZnLgCLcBGAs/s1600/instagram.png" title="Sígueme en Instagram"/> adler.luft </a><br>